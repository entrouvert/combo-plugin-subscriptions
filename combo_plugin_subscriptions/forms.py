import logging

from django import forms

logger = logging.getLogger(__name__)

class SubscriptionsManageForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.user = self.request.user
        instance = self.instance = kwargs.pop('instance')
        try:
            subscriptions = self.subscriptions = self.instance.get_subscriptions(user=self.user.email)
        except:
            subscriptions = self.subscriptions = []
            logger.warning('unable to retrieve subscriptions for %r: %r',
                    self.user, self.user.email, exc_info=True)
        super(SubscriptionsManageForm, self).__init__(*args, **kwargs)
        self.all_choices = set()
        for subscription in subscriptions:
            if not instance.check_resource(subscription['name']):
                continue
            choices = []
            initial = []
            for transport in subscription['transports']['available']:
                if not instance.check_transport(transport):
                    continue
                self.all_choices.add(transport)
                choices.append((transport, transport))
                if transport in subscription['transports']['defined']:
                    initial.append(transport)
            self.fields[instance.simplify(subscription['name'])] = \
                    forms.MultipleChoiceField(label=subscription['description'],
                            choices=choices, initial=initial,
                            widget=forms.CheckboxSelectMultiple,
                            required=False)

    def save(self):
        self.full_clean()
        subscriptions = []
        for key, value in self.cleaned_data.iteritems():
            subscriptions.append({
                'name': key,
                'transports': value
            })
        try:
            self.instance.set_subscriptions(subscriptions, user=self.user.email)
        except:
            subscriptions = self.subscriptions = []
            logger.warning('unable to save subscriptions for %r: %r',
                    self.user, self.user.email, exc_info=True)
