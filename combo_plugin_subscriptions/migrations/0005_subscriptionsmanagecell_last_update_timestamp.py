# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('combo_plugin_subscriptions', '0004_subscriptionsmanagecell_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriptionsmanagecell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2017, 2, 14, 20, 5, 45, 969207, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
