# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('combo_plugin_subscriptions', '0003_subscriptionsmanagecell_restricted_to_unlogged'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriptionsmanagecell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
