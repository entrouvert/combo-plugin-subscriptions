# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('combo_plugin_subscriptions', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriptionsmanagecell',
            name='passerelle_url',
            field=models.URLField(max_length=128, verbose_name='Subscriptions service url'),
            preserve_default=True,
        ),
    ]
