# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0005_auto_20150226_0903'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionsManageCell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('passerelle_url', models.URLField(max_length=128, verbose_name='Subscriptions service url', choices=[(b'https://passerelle.montpellier-agglo.com/register/newsletter/json', b'Passerelle Montpellier Prod'), (b'https://passerelle-test-entrouvert.montpellier3m.fr/register/newsletter/json', b'Passerelle Montpellier Recette')])),
                ('resources_restrictions', models.TextField(verbose_name='resources restrictions', blank=True)),
                ('transports_restrictions', models.TextField(verbose_name='transports restrictions', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page')),
            ],
            options={
                'verbose_name': 'Manage your subscriptions',
            },
            bases=(models.Model,),
        ),
    ]
