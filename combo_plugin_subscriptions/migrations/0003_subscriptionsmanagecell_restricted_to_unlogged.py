# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('combo_plugin_subscriptions', '0002_auto_20150417_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriptionsmanagecell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
    ]
